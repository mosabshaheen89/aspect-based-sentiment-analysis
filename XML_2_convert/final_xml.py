from xml.etree import ElementTree
from xml.dom import minidom

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

from xml.etree.ElementTree import Element, SubElement, Comment
from ElementTree_pretty import prettify
import re

f = open("test.raw", "r")
lines = f.read().splitlines()
sentences = Element('sentences')
i=0
while i<len(lines):
    sentence = SubElement(sentences, 'sentence')
    new_line = str.replace(lines[i], '$T$', lines[i+1])
    text = SubElement(sentence, 'text')
    text.text = new_line
    aspectTerms = SubElement(sentence, 'aspectTerms')
    stat = [(m.start(),m.end()) for m in re.finditer(lines[i+1], new_line)]
    if lines[i+2]=='-1':
        for j in range(0, len(stat)):
            aspectTerm = SubElement(aspectTerms, 'aspectTerm term="{}" polarity="negative" from="{}" to="{}"'
                                    .format(lines[i+1], stat[j][0], stat[j][1]))
    if lines[i+2]=='0':
        for j in range(0, len(stat)):
            aspectTerm = SubElement(aspectTerms, 'aspectTerm term="{}" polarity="neutral" from="{}" to="{}"'
                                    .format(lines[i+1], stat[j][0], stat[j][1]))
    if lines[i+2]=='1':
        for j in range(0, len(stat)):
            aspectTerm = SubElement(aspectTerms, 'aspectTerm term="{}" polarity="positive" from="{}" to="{}"'
                                    .format(lines[i+1], stat[j][0], stat[j][1]))
    aspectCategories = SubElement(sentence, 'aspectCategories')
    aspectCategory = SubElement(aspectCategories, 'aspectCategory category="NA" polarity="NA"')
    i=i+3
# print(prettify(sentences))
with open("Twitter_test.xml",'w+') as f:
    f.write(prettify(sentences))
